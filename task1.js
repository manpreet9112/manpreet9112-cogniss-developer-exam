const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputFile = "input2.json";
const outputFile = "output2.json";

var output = {};
jsonfile.readFile(inputFile,function(error,body){
    console.log("loaded input file content", body.names);
    output.emails = [];
    var arr = [];
    arr = body.names;
   for(i =0; i<arr.length;i++){
     var str = arr[i].split('').reverse().join('');
     console.log("reading reverse string " +str);
     str += randomstring.generate(5);
     output.emails.push(str+= "@gmail.com")
     jsonfile.writeFile(outputFile, output, {spaces: 2}, function(error) {
       console.log("All done!");
     });
   }
    console.log(arr);   
});

